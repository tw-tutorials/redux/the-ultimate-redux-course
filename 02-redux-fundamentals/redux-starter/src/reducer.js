import * as actions from './actionTypes';

const initState = [];
let lastId = 0;

export default function reducer(state = initState, action) {
	switch (action.type) {
		case actions.BUG_ADDED:
			return [
				...state,
				{
					id: ++lastId,
					description: action.payload.description,
					resolved: false
				}
			];

		case actions.BUG_REMOVED:
			return state.filter((bug) => bug.id !== action.payload.id);

		case actions.BUG_RESOLVED:
			return state.map(
				(bug) =>
					bug.id !== action.payload.id
						? bug // return bug
						: { ...bug, resolved: true } // set 'resolved' to true
			);

		default:
			return state;
	}
}
